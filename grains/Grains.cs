﻿using System;

public static class Grains
{
    // 1 square = 1; 3 square = 4; 4 square = 8; 5 square = 16; 6 square = 32 7 square = 64 8 square 128;
    public static ulong Square(int n)
    {
        sanitizer(n);
        ulong j = 1;
        for (int i=1; i < n; i++)
        {
            if (n == 1)
                return 1;
            if (n == 2)
                return 2;
            if (j == 1)
            {
				j = j * 2;
                continue;

            }
            j = j * 2;
        }
        return (ulong)j;

    }

    

    public static ulong Total()
    {
        ulong btotal = 0;
        for (int i = 1; i <= 64; i++)
        {
            btotal = btotal + Square(i);
        }
        return btotal;
    }

    public static void sanitizer(int n)
    {
        if (n == 0 || n < 0 || n > 64)
            throw new ArgumentOutOfRangeException("You are wrong");
    }
}
