﻿using System;
using System.Collections;

using System.Collections.Generic;

public static class Pangram
{
    public static bool IsPangram(string input)
    {
        string myWord = input.Replace(" ", "");
        myWord = myWord.Replace("-", "");
        myWord = myWord.ToLower();
        var alpha = "abcdefghijklmnopqrstuvwxyz";
        foreach (char a in alpha)
        {
            if (!(myWord.Contains(a)))
                return false;
        }
        return true;
    }
}

