using System;
using System.Collections.Generic;

public static class RnaTranscription
{
    public static string ToRna(string nucleotide)
    {
        Dictionary<string, string> sequence = new Dictionary<string, string>();
        sequence.Add("G", "C");
        sequence.Add("C", "G");
        sequence.Add("T", "A");
        sequence.Add("A", "U");
        string RNA = "";
        if (nucleotide.Length < 1)
            return "";
        else if (nucleotide.Length == 1)
        {
            sequence.TryGetValue(nucleotide, out RNA);
            return RNA;
        }
        else
        {
            string t = "";
            char[] nukeArg = nucleotide.ToCharArray();
            List<string> allResults = new List<string>();
            string results;
            foreach (var s in nukeArg)
            {
                sequence.TryGetValue(s.ToString(), out results);
                //nukeList.Add(results);
                t += results;
            }
            return t;
        }
    }
}