﻿public static class TwoFer
{
    public static string Speak(string args = "")
    {
       if (args.Length > 0)
	   {
		   return $"One for {args}, one for me";
	   }
	   
	  else
	   {
			return "One for you, one for me.";
	   }
    }
	
}
